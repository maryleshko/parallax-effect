import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var seaImageView: UIImageView!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addParallaxToView(view: seaImageView, magnitude: 30)
        self.textLabel.text = "Hello"
        self.textLabel.text = "Another hello"
        print("Hello")
        print("Add new branch anton-develop")
        print("My name is Alex")
        print("Hi")
    }
    
     func addParallaxToView(view: UIView, magnitude: Float) {
           let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
           horizontal.minimumRelativeValue = -magnitude
           horizontal.maximumRelativeValue = magnitude

           let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
           vertical.minimumRelativeValue = -magnitude
           vertical.maximumRelativeValue = magnitude

           let group = UIMotionEffectGroup()
           group.motionEffects = [horizontal, vertical]
           view.addMotionEffect(group)
       }
}

